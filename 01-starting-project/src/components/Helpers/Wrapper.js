//wrapper class - wrapping adjacent jsx elements is wrong 
//and wrapping in div that will be rendered is wrong too
//we want to render only what we must have
//wrapper returns only 1 element which inside have many jsx to be rendered
//we dont have to create it, it's called React.Fragment
const Wrapper = props =>{
    return props.children;
};

export default Wrapper;